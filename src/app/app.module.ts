import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

// Reactive Form
import { ReactiveFormsModule } from '@angular/forms';

// App routing modules
import { AppRoutingModule } from './shared/routing/app-routing.module';

// App components
import { AppComponent } from './app.component';

// Firebase services + enviorment module
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

// Auth service
import { AuthService } from './shared/services/auth/auth.service';

// Angular
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Otras dependencias
import Swal from 'sweetalert2';

// Config token
import { ALERT_TOKEN, SwalAlertAdapter } from './config-token';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CoreModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ReactiveFormsModule,
    FlexLayoutModule,
  ],
  providers: [
    AuthService,
    {
      provide: ALERT_TOKEN,
      useFactory: () => {
        const alert = new SwalAlertAdapter();
        alert.config(Swal);
        return alert;
      }
    },
    {
      provide: APP_INITIALIZER,
      useFactory: getUser(),
      multi: true,
      deps: [AuthService]
    },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
function getUser(): Function {
  return (auth: AuthService) => {
    return () => {
      const user = JSON.parse(localStorage.getItem('user'));
      auth.userData = user;
      auth.user$.next(user);
    };
  };
}

