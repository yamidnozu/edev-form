export const pathsRoutes = {
    NONE: '',
    // =================== auth ======================
    AUTH: 'auth',
    SIGN_IN: 'sign-in',
    REGISTER_FORM: 'register-form',
    VERIFY_EMAIL: 'verify-email-address',
    FORGOT_PASSWORD: 'forgot-password',
    // =================== dashboard ======================
    DASHBOARD: 'dashboard',

    // =================== forms ======================
    FORMS: 'forms',
    INFO_FORMS: 'info',
    FORM_FORMS: 'form',
};

export const appRoutes = {
    NONE: '',
    // =================== auth ======================
    AUTH: `${pathsRoutes.AUTH}`,
    SIGN_IN: `${pathsRoutes.AUTH}/${pathsRoutes.SIGN_IN}`,
    REGISTER_FORM: `${pathsRoutes.AUTH}/${pathsRoutes.REGISTER_FORM}`,
    VERIFY_EMAIL: `${pathsRoutes.AUTH}/${pathsRoutes.VERIFY_EMAIL}`,
    FORGOT_PASSWORD: `${pathsRoutes.AUTH}/${pathsRoutes.FORGOT_PASSWORD}`,

    // =================== dashboard ======================
    DASHBOARD: `${pathsRoutes.DASHBOARD}`,

    // =================== forms ======================
    FORMS: `${pathsRoutes.DASHBOARD}/${pathsRoutes.FORMS}`,
    INFO_FORMS: `${pathsRoutes.DASHBOARD}/${pathsRoutes.FORMS}/${pathsRoutes.INFO_FORMS}`,
    FORM_FORMS: `${pathsRoutes.DASHBOARD}/${pathsRoutes.FORMS}/${pathsRoutes.FORM_FORMS}`,
};
