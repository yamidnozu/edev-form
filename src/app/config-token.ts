
import { InjectionToken } from '@angular/core';
import { AlertAdapter } from './shared/services/alert/alert.service';
import Swal, { SweetAlertOptions } from 'sweetalert2';
import { Subject, Observable } from 'rxjs';

export const ALERT_TOKEN = new InjectionToken<any>('ALERT');

export class SwalAlertAdapter implements AlertAdapter {
    public alert: any;
    config(alert: any): void {
        this.alert = alert;
    }

    show(options?) {
        this.alert.fire(options);
    }

    showText(text: string) {
        this.alert.fire(text);
    }

    showToast(options: SweetAlertOptions = { icon: 'success', title: 'Hecho' }) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

        Toast.fire(options);
    }

    showConfirm(options?: SweetAlertOptions): Observable<boolean> {
        let optionsDefault = {
            title: '¿Continuar con la operación?',
            text: 'No podrás revertir esta operación',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí!',
            cancelButtonText: 'Cancelar!',
            reverseButtons: true
        }

        if (options) {
            options = { ...optionsDefault, ...options } as SweetAlertOptions;
        }
        const response = new Subject<boolean>();
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        });

        swalWithBootstrapButtons.fire(options)
            .then((result) => {
                if (result.value) {
                    response.next(true);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    response.next(false);
                }
            });
        return response.asObservable();
    }


}
