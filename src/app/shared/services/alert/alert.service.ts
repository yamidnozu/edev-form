import { Injectable, Inject } from '@angular/core';
import { ALERT_TOKEN } from '../../../config-token';
import { Observable } from 'rxjs';

/**
 * Clase abstracta para implementación de servicios
 */
export abstract class AlertAdapter {
  /**
   * @description Se usa para configuración inicial de las alertas
   * @param alert Configuración de servicio a usar para
   * las alertas
   * @example En app.module.ts {
      provide: ALERT_TOKEN,
      useFactory: () => {
        const alert = new SwalAlertAdapter();
        alert.config(Swal);
        return alert;
      }
    }
   */
  public abstract config(alert);
  public abstract show(options?);
  public abstract showText(text?);
  public abstract showToast(options?);
  public abstract showConfirm(options?);
}


@Injectable({
  providedIn: 'root'
})
export class AlertService {
  constructor(@Inject(ALERT_TOKEN) private alert: AlertAdapter) {

  }

  show(options?) {
    this.alert.show(options);
  }

  showText(text: string) {
    this.alert.showText(text);
  }

  showToast(options) {
    this.alert.showToast(options);
  }

  showConfirmDialog(options?): Observable<boolean> {
    return this.alert.showConfirm(options);
  }
}


