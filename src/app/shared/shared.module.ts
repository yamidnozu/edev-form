import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { SharedRoutingModule } from './shared-routing.module';
import { TableDynamicComponent } from './ui/table-dynamic/table-dynamic.component';


@NgModule({
  declarations: [
    TableDynamicComponent,

  ],
  imports: [
    CommonModule,
    SharedRoutingModule,

    /** ======================= material ====================== */
    MaterialModule
  ],
  exports: [
    TableDynamicComponent,
  ]
})
export class SharedModule { }
