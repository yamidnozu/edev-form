import { Component, ViewChild, Input, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { MatTableDataSource, MatTable } from '@angular/material/table';

@Component({
  selector: 'wkr-table-dynamic',
  templateUrl: './table-dynamic.component.html',
  styleUrls: ['./table-dynamic.component.scss']
})
export class TableDynamicComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) matTable: MatTable<any>;

  @Input() inColumns$: Observable<{ id: string, description: string }[]>;
  @Input() inData$: Observable<any[]>;

  @Input() inTitle = '';
  @Input() inRemove = false;

  @Output() outSelected = new EventEmitter<any>();
  @Output() outRemove = new EventEmitter<any>();

  displayedColumns$ = new BehaviorSubject([]);
  dataSource = new MatTableDataSource([]);

  ngOnInit(): void {
    this.listenInfoTable();
  }

  private listenInfoTable() {
    combineLatest([this.inColumns$, this.inData$])
      .subscribe(this.eatCombine());
  }

  private eatCombine(): (value: [{ id: string; description: string; }[], any[]]) => void {
    return ([m, d]) => {
      if (m && d) {
        this.updateTableAndColumns(m, d);
      } else {
        this.resetTableAndColumns(m, d);
      }
    };
  }

  // Reset table
  private resetTableAndColumns(m: { id: string; description: string; }[], d: any[]) {
    if (m === null || m === undefined && d === null || d === undefined) {
      this.dataSource = new MatTableDataSource([]);
      this.displayedColumns$.next([]);
      this.matTable?.renderRows();
    }
  }

  // Asiggn datasource and displayed columns
  private updateTableAndColumns(m: { id: string; description: string; }[], d: any[]) {
    const displayedColumns = m.map(n => n.id).filter((r: any) => r !== 'tmpId' && r !== 'id');
    if (this.inRemove) {
      displayedColumns.push('remove');
    }
    this.displayedColumns$.next(displayedColumns);
    this.dataSource = new MatTableDataSource(d);
    this.matTable?.renderRows();
  }

  columns(columns: any[]) {
    return columns ? columns : null;
    /*.filter(
      (r: any) => r.id !== 'tmpId' && r.id !== 'id'
    )*/
  }

  onRemove(row) {
    this.outRemove.emit(row);
  }

  onDblClick(row) {
    this.outSelected.emit(row);
  }
}
