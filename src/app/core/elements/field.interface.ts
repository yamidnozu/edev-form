export interface Validator {
  name: string;
  validator: any;
  message: string;
}
export interface FieldConfig {
  label?: string;
  name?: string;
  inputType?: 'text' | 'datetime-local' | 'month' | 'time' | 'week' | 'color' | 'number';
  options?: { id: number|string, description: string }[];
  collections?: any;
  type: string;
  value?: any;
  validations?: Validator[];
}
