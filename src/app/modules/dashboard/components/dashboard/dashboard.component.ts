import { Component, OnInit, NgZone } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { appRoutes } from 'src/app/routes';


@Component({
  selector: 'wkr-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  get routeInfoUser() { return '/' + appRoutes.INFO_FORMS; };
  get routeFormUser() { return '/' + appRoutes.FORM_FORMS; };

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone
  ) { }

  ngOnInit() { }

  onLogout() {
    this.authService.signOut();
  }

}
