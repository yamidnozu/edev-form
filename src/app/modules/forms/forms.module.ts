/** ============================ modulos =========================== */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DynamicFormsModule } from '../../core/elements/dynamic-forms.module';
import { MaterialModule } from '../../shared/material.module';
import { SharedModule } from '../../shared/shared.module';
import { FormsRoutingModule } from './forms-routing.module';

/** ============================ componentes =========================== */
import { FormComponent } from './components/form/form.component';
import { InfoComponent } from './components/info/info.component';
import { ListComponent } from './components/list/list.component';

import { NewFormState } from './services/new-form.state';

@NgModule({
  declarations: [
    InfoComponent,
    FormComponent,
    ListComponent,

  ],
  imports: [
    CommonModule,
    FormsRoutingModule,
    SharedModule,
    MaterialModule,
    FlexLayoutModule,
    DynamicFormsModule,
  ],
  providers: [
    NewFormState
  ]
})
export class FormsModule { }
