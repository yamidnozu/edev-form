import { Injectable } from '@angular/core';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import { Store } from '../../../core/helpers/store';
import { FormsModel, FormsService } from './forms.service';
import { BodyFormModel } from '../models/forms.model';
import { of } from 'rxjs';


export interface FormEstadoModel {
    // Forms
    forms: BodyFormModel[];
    headersData: { id: string, description: string }[];
}
const estadoDefault: FormEstadoModel = {
    // Forms
    forms: null,
    headersData: null,
};
@Injectable({
    providedIn: 'root'
})
export class FormState extends Store<FormEstadoModel> {

    readonly forms$ = this.state$.pipe(map(el => el.forms), distinctUntilChanged());
    readonly headersData$ = this.state$.pipe(map(el => el.headersData), distinctUntilChanged());

    constructor(
        private formService: FormsService<FormsModel>,
    ) { super(estadoDefault); }

    /** ======================= data forms ================== */
    set forms(forms: BodyFormModel[]) { this.setState({ ...this.state, forms: forms }); }
    get forms(): BodyFormModel[] { return this.state.forms; }

    addForm(form) { return this.formService.setForms(form); }

    removeForm(form: BodyFormModel) { return this.formService.remove(form); }

    getForms() {
        this.formService.getForms()
            .pipe(this.assignHeaderDataFromData())
            .subscribe((data: any) => this.forms = data);
    }


    /** ======================= data header table ================== */

    set headersData(headersData: any) { this.setState({ ...this.state, headersData }); }
    get headersData(): any { return this.state.headersData; }

    private assignHeaderDataFromData() {

        this.headersData = [
            { id: 'name', description: 'Nombre' },
            { id: 'description', description: 'Descripción' },
        ];
        return tap(r => r);
    }

}


        // return tap((el: BodyFormModel[]) => {
        //     if (el) {
        //         const headers = {};
        //         el.forEach(e => {
        //             const field = Object.keys(e);
        //             field.forEach(e => headers[e] = { id: e, description: e });
        //         });
        //         const final = [];
        //         Object.keys(headers).forEach(f => final.push({ id: f, description: f }));
        //         this.headersData = final;
        //     }
        // });
