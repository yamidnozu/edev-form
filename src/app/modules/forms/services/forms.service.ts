import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { firestore } from 'firebase';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Timestamp } from 'rxjs/internal/operators/timestamp';
import { BodyFormModel } from '../models/forms.model';
import { map } from 'rxjs/operators';

export interface FormsModel {
    nombreusuario: string;
    email: string;
    genero: string;
    fechanacimiento: firestore.Timestamp;
    pais: string;
    terminoscondiciones: boolean;
}

export enum collectionEnum {
    USER_REG = 'user_reg',
    FORMS = 'wforms',
}

@Injectable({
    providedIn: 'root'
})
export class FormsService<T> {
    constructor(
        public afs: AngularFirestore,   // Inject Firestore service
        private authService: AuthService,
    ) { }

    get REFBD() {
        return this.afs
            .collection<BodyFormModel>(collectionEnum.USER_REG)
            .doc(this.authService.userData.uid)
            .collection<BodyFormModel>(collectionEnum.FORMS);
    }

    /**
     * Add user in wforms collection
     * @param form
     */
    setForms(form: T) {
        const response = new Subject();
        const id = this.afs.createId();
        form['id'] = id;
        const formRef: AngularFirestoreCollection<T> = this.afs.collection(collectionEnum.USER_REG);
        formRef
            .doc(id)
            .set(form)
            .then(e => response.next(true))
            .catch(e => response.error(e));
        return response.asObservable();
    }

    getForms(): Observable<BodyFormModel[]> {
        return this.REFBD
            .valueChanges()
            .pipe(map(r => r as BodyFormModel[]));
    }

    saveForm(payload: BodyFormModel) {
        const response = new Subject();
        if (!payload.id) {
            const id = this.afs.createId();
            const uid = this.authService.userData.uid;
            payload['id'] = id;
            payload['uid'] = uid;
            payload['create'] = firestore.FieldValue.serverTimestamp();
        }
        payload['lastUpdate'] = firestore.FieldValue.serverTimestamp();
        this.REFBD
            .doc(payload.id)
            .set(payload, { merge: true })
            .then(e => response.next(true))
            .catch(e => response.error(e));
        return response.asObservable();
    }

    remove(payload: BodyFormModel): Observable<boolean> {
        const response = new Subject<boolean>();
        if (payload.id) {
            this.REFBD
                .doc(payload.id).delete()
                .then(e => response.next(true))
                .catch(e => response.error(e));
        } else {
            response.next(false);
        }
        return response.asObservable();
    }

}
