import { Injectable } from '@angular/core';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { Store } from '../../../core/helpers/store';
import { FormsModel, FormsService } from './forms.service';
import { BehaviorSubject } from 'rxjs';
import { NewFormStateModel, FieldFormModel, InfoFormModel, BodyFormModel } from './../models/forms.model';

const estadoDefault: NewFormStateModel = {
    // Forms
    headersData: null,
    body: null,
};
@Injectable()
export class NewFormState extends Store<NewFormStateModel> {

    readonly inputEditing$ = new BehaviorSubject(null);
    readonly fieldsForm$ = this.state$.pipe(map(el => el?.body?.fields), distinctUntilChanged());
    readonly headersData$ = this.state$.pipe(map(el => el.headersData), distinctUntilChanged());
    readonly infoForm$ = this.state$.pipe(map(el => ({ name: el.body.name, description: el.body.description })), distinctUntilChanged());

    readonly body$ = this.state$.pipe(map(el => el.body), distinctUntilChanged());
    set body(body: BodyFormModel) {
        this.assignHeaderDataFromData(body?.fields || []);
        this.setState({ ...this.state, body });
    }
    get body(): BodyFormModel { return this.state.body; }

    constructor(
        private formService: FormsService<FormsModel>,
    ) { super(estadoDefault); }

    /** ======================= field forms ================== */
    set fieldsForm(fieldsForm: FieldFormModel[]) { this.setState({ ...this.state, body: { ...this.state?.body, fields: fieldsForm } }); }
    get fieldsForm(): FieldFormModel[] { return this.state?.body?.fields; }

    addField(form) {
        console.log('Pasa por aqui');

        let fields = this.body?.fields as FieldFormModel[] || null;
        if (/*form.id || */form.tmpId) {
            const idx = fields.findIndex(f => /*f.id === form.id || */f.tmpId === form.tmpId);
            fields[idx] = form;
        } else {
            if (!fields) { fields = []; }
            form = { ...form, tmpId: new Date().getTime() };
            fields.push(form);
        }
        this.fieldsForm = fields;
        this.assignHeaderDataFromData(fields);
    }

    /** =========================== info new form ====================== */
    set infoForm(infoForm: InfoFormModel) { this.setState({ ...this.state, body: { ...this.state.body, ...infoForm } }); }
    get infoForm(): InfoFormModel { return { name: this.state?.body?.name, description: this.state?.body?.description }; }

    sendEdit(field) {
        this.inputEditing$.next(field);
    }


    /** ======================= data header table ================== */

    set headersData(headersData: any) { this.setState({ ...this.state, headersData }); }
    get headersData(): any { return this.state.headersData; }

    private assignHeaderDataFromData(fields: any[]) {
        if (fields) {
            const headers = {};
            fields.forEach(_field => {
                const field = Object.keys(_field);
                field.forEach(e => headers[e] = { id: e, description: e });
            });
            const final = [];
            Object.keys(headers).forEach(f => final.push({ id: f, description: f }));
            this.headersData = final;
        }
    }


    /** ========================== save new form ===================================== */
    saveForm() {
        return this.formService.saveForm(this.state.body);
    }


    clean() {
        this.setState(estadoDefault);
    }

}
