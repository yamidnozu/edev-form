import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { FormState } from '../../services/forms.state';



@Component({
  selector: 'wkr-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() inColumns$: Subject<{ id: string, description: string }[]> = new BehaviorSubject([]);
  @Input() inData$: Subject<any[]> = new BehaviorSubject([]);

  @Input() inTitle = '';
  @Input() inRemove = false;

  @Output() outSelected = new EventEmitter<any>();
  @Output() outRemove = new EventEmitter<any>();

  constructor(
  ) { }

  ngOnInit(): void {
  }

  onSelected(row) {
    this.outSelected.emit(row);
  }

  onRemove(row) {
    this.outRemove.emit(row);
  }

}
