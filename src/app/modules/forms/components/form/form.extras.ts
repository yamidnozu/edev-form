import { Validators } from '@angular/forms';
import { emailRegExp, textoRegExp } from './../../../../core/helpers';
import { FieldConfig } from './../../../../core/elements/field.interface';


// export const regConfig: FieldConfig[] = [
//     {
//         type: 'input',
//         label: 'Nombre de usuario',
//         inputType: 'text',
//         name: 'Nombre usuario',
//         validations: [
//             {
//                 name: 'required',
//                 validator: Validators.required,
//                 message: 'Campo requerido'
//             },
//             {
//                 name: 'pattern',
//                 validator: Validators.pattern(textoRegExp),
//                 message: 'Solo texto'
//             }
//         ]
//     },
//     // {
//     //     type: 'input',
//     //     label: 'Email personal',
//     //     inputType: 'email',
//     //     name: 'email',
//     //     validations: [
//     //         {
//     //             name: 'required',
//     //             validator: Validators.required,
//     //             message: 'Campo requerido'
//     //         },
//     //         {
//     //             name: 'pattern',
//     //             validator: Validators.pattern(
//     //                 emailRegExp
//     //             ),
//     //             message: 'Email inválido'
//     //         }
//     //     ]
//     // },
//     // {
//     //     type: 'radiobutton',
//     //     label: 'Género',
//     //     name: 'genero',
//     //     options: ['Masculino', 'Femenino'],
//     //     value: 'Male'
//     // },
//     // {
//     //     type: 'date',
//     //     label: 'Fecha de nacimiento',
//     //     name: 'fechanacimiento',
//     //     validations: [
//     //         {
//     //             name: 'required',
//     //             validator: Validators.required,
//     //             message: 'Fecha de nacimiento requerida'
//     //         }
//     //     ]
//     // },
//     // {
//     //     type: 'select',
//     //     label: 'País',
//     //     name: 'pais',
//     //     value: 'CO',
//     //     options: ['Colombia', 'México', 'Perú', 'Venezuela', 'Panamá']
//     // },
//     // {
//     //     type: 'checkbox',
//     //     label: 'Acepta términos y condiciones',
//     //     name: 'terminoscondiciones',
//     //     value: true
//     // },
//     {
//         type: 'button',
//         label: 'Guardar'
//     }
// ];




export const regConfig: FieldConfig[] = [
    {
        type: 'input',
        label: 'Nombre de campo',
        inputType: 'text',
        name: 'name',
        validations: [
            {
                name: 'required',
                validator: Validators.required,
                message: 'Campo requerido'
            },
            {
                name: 'pattern',
                validator: Validators.pattern(textoRegExp),
                message: 'Solo texto'
            }
        ]
    },
    {
        type: 'select',
        label: 'Tipo de campo',
        name: 'field',
        value: 'input',
        options: [
            { id: 'input', description: 'Input' },
            { id: 'radiobutton', description: 'Radiobutton' },
            { id: 'date', description: 'Date' },
            { id: 'select', description: 'Select' },
            { id: 'checkbox', description: 'Checkbox' },
            { id: 'button', description: 'Button' },
        ]
    },
    {
        type: 'button',
        label: 'Agregar'
    }
];
export const regConfigInfoForm: FieldConfig[] = [
    {
        type: 'input',
        label: 'Nombre del formulario',
        inputType: 'text',
        name: 'name',
        validations: [
            {
                name: 'required',
                validator: Validators.required,
                message: 'Campo requerido'
            }
        ]
    },
    {
        type: 'input',
        label: 'Descripción',
        inputType: 'text',
        name: 'description',
    },
    {
        type: 'checkbox',
        label: 'Activo',
        name: 'activo',
        value: true
    },

];