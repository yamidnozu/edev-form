import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicFormComponent } from 'src/app/core/elements/components';
import { FieldConfig } from 'src/app/core/elements/field.interface';
import { FormState } from './../../services/forms.state';
import { regConfig, regConfigInfoForm } from './form.extras';
import { AlertService } from '././../../../../shared/services/alert/alert.service';
import { NewFormState } from '../../services/new-form.state';
import { BodyFormModel } from '../../models/forms.model';


@Component({
  selector: 'wkr-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @ViewChild('infoForm', { static: true }) infoForm: DynamicFormComponent;
  @ViewChild('infoField', { static: true }) fieldForm: DynamicFormComponent;

  regConfig: FieldConfig[] = regConfig;
  regConfigInfoForm: FieldConfig[] = regConfigInfoForm;

  constructor(
    public formsState: FormState,
    public newFormsState: NewFormState,
    public alert: AlertService
  ) { }

  ngOnInit(): void {
    // Get data from service data
    this.formsState.getForms();

    // Observables
    this.detectValueChangesInfoForm();
    this.detectChangesInfoForm();
  }

  submit($event) {
    this.newFormsState.addField($event);
  }

  onSelected(row) {
    this.newFormsState.sendEdit(row);
  }

  onFormSelected(row: BodyFormModel) {
    this.newFormsState.body = row;
  }

  onSaveForm() {
    const infoForm = this.infoForm.form;
    if (infoForm.valid) {
      this.newFormsState.infoForm = infoForm.value;
      this.sendSave();
    } else {
      infoForm.markAllAsTouched();
      this.alert.showToast({ title: 'Completa los campos', icon: 'info' });
    }
  }

  onRemove(row: BodyFormModel) {
    console.log('Removing', row);
    this.alert.showConfirmDialog(
      {
        title: '¿Seguro?',
        text: 'No podrá deshacer esta operación'
      }
    ).subscribe(response => {
      if (response) {
        this.formsState.removeForm(row).subscribe(resp => {
          if (resp) {
            this.alert.showToast({ title: 'Formulario eliminado exitosamente', icon: 'success' });
          }
        });
      }
    });

  }

  private sendSave() {
    this.newFormsState.saveForm().subscribe(response => this.resetFormsAndStates());
  }

  private resetFormsAndStates() {
    this.alert.showToast({ title: 'Formulario almacenado exitosamente', icon: 'success' });
    this.infoForm.form.reset();
    this.fieldForm.form.reset();
    this.newFormsState.clean();
  }

  private detectValueChangesInfoForm() {
    setTimeout(() => {
      this.infoForm?.form?.valueChanges.subscribe(value => this.newFormsState.infoForm = value);
    }, 100);
  }

  private detectChangesInfoForm() {
    this.newFormsState.body$
      .subscribe(f => {
        if (f) {
          this.infoForm.form.patchValue(f, { emitEvent: false });
        }
      });
    this.newFormsState.inputEditing$
      .subscribe(f => {
        if (f) {
          this.fieldForm.form.patchValue(f, { emitEvent: false });
        }
      });
  }

}
