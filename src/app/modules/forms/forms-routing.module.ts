import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './components/info/info.component';
import { FormComponent } from './components/form/form.component';

const routes: Routes = [
  { path: 'info', component: InfoComponent, },
  { path: 'form', component: FormComponent, },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
