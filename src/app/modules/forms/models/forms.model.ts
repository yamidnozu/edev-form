export interface FieldFormModel {
    id?: string;
    name: string;
    field: string;
    tmpId?: number;
}

export interface InfoFormModel {
    description: string;
    name: string;
}
export interface BodyFormModel {
    id: string;
    uid: string;
    // ===== InfoFormModel ======
    description: string;
    name: string;
    // ===== Fields =============
    fields: FieldFormModel[];
}
export interface NewFormStateModel {
    headersData: { id: string, description: string }[];
    body: BodyFormModel;
}
