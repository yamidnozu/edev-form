import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../shared/services/auth/auth.service';
import { appRoutes } from 'src/app/routes';


@Component({
  selector: 'wkr-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent implements OnInit {

  get routeRegisterUser() { return '/' + appRoutes.REGISTER_FORM; };
  get routeForgotPasswors() { return '/' + appRoutes.FORGOT_PASSWORD; };

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit() { }

}
